# btlock

Locks your gnome desktop when when you run away from your PC. Uses the signal strength (RSSI) for the bluetooth of your mobile phone.

## Installation

* sudo apt install bluetooth python3-bluez gnome-screensaver
* Install the rssi function, see instructions in https://github.com/ewenchou/bluetooth-proximity.git.
* Pair you mobile phone bluetooth with your linux pc
* Determine your phone bluetooth mac address by running:
    python list-nearby-bluetooth-devices.py
* Create bt_mac.txt and add the bluetooth mac address of your phone
* Try btlock with ./btlock.py
* you may create a launcher - btlock.desktop for example, and use Tweaks app to set btlock to run automatically at login
