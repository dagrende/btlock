#!/usr/bin/env python3
import os
import signal
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
from gi.repository import Gtk, AppIndicator3, GLib
import time
from threading import Thread
from bt_proximity import BluetoothRSSI
import subprocess
from datetime import datetime

class Indicator():
    def __init__(self):
        self.app = 'rssi-indicator'
        self.app_dir = os.path.dirname(os.path.abspath(__file__))

        self.indicator = AppIndicator3.Indicator.new(
            self.app, self.app_dir + "/btlock_icon.svg",
            AppIndicator3.IndicatorCategory.OTHER)
        self.indicator.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
        self.indicator.set_menu(self.create_menu())

        # init state
        self.isLocked = False
        self.rssi_near = -5
        self.rssi_far = -12
        self.lockDelay = 6
        self.unlocked_by_bt = True   # true if was unlocked by bluetooth - not by password
        self.timeToLock = self.lockDelay

        # read your phone bt mac addresss from file to not reveal it by mistake in the cloud
        with open(self.app_dir + '/bt_mac.txt', 'r') as file:
            self.bt_mac = file.read().strip()
            file.close()
        self.btrssi = BluetoothRSSI(addr=self.bt_mac)

        # the thread:
        self.update = Thread(target=self.measure_loop)
        self.update.setDaemon(True)   # daemonize the thread to make the indicator stopable
        self.update.start()


    def create_menu(self):
        menu = Gtk.Menu()
        # quit
        item_quit = Gtk.MenuItem(label='Quit')
        item_quit.connect('activate', self.stop)
        menu.append(item_quit)

        menu.show_all()
        return menu

    def lock(self):
        os.system("echo lock")
        os.system("gnome-screensaver-command -l")

    def unlock(self):
        os.system("echo unlock")
        os.system("gnome-screensaver-command -d")

    def screen_is_locked(self):
        return not subprocess.check_output(['gnome-screensaver-command', '-q']).decode('utf-8').strip().endswith(' inactive')

    def lid_is_open(self):
        with open('/proc/acpi/button/lid/LID/state') as f:
            return 'open' in f.read()

    def analyze_and_set_lock_state(self, rssi):
        isFar = rssi <= self.rssi_far
        isNear = rssi >= self.rssi_near
        self.isLocked = self.screen_is_locked()

        if self.isLocked:
            if isNear:
                self.unlock()
                self.isLocked = False
                self.unlocked_by_bt = True
                self.timeToLock = self.lockDelay
        else:
            if isNear:
                self.timeToLock = self.lockDelay
                if not self.unlocked_by_bt:
                    self.unlocked_by_bt = True
            if isFar and self.unlocked_by_bt:
                if self.timeToLock <= 0:
                    self.lock()
                    self.isLocked = True
                    self.unlocked_by_bt = False
                else:
                    self.timeToLock -= 1

    def measure_loop(self):
        prev_lid = self.lid_is_open()
        while True:
            time.sleep(1)
            try:
                lid = self.lid_is_open()
                if (prev_lid != lid):
                    if lid:
                        print('open lid')
                    else:
                        print('close lid')
                    prev_lid = lid

                rqresult = self.btrssi.request_rssi()
                if rqresult:
                    rssi = rqresult[0]
                else:
                    print('rqresult = None')
                    rssi = 0

                #print("tick", datetime.now().strftime("%H:%M:%S"))
                message = "RSSI=" + str(rssi)
                if self.timeToLock < self.lockDelay:
                    message = message + " " + str(self.timeToLock)
                GLib.idle_add(
                    self.indicator.set_label,
                    message,
                    self.app,
                    priority=GLib.PRIORITY_DEFAULT
                    )
                self.analyze_and_set_lock_state(rssi)
            except Exception as e:
                print('exc: ' + e)

    def stop(self, source):
        Gtk.main_quit()

Indicator()
signal.signal(signal.SIGINT, signal.SIG_DFL)
Gtk.main()
